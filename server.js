var express = require('express')
  , bodyParser = require('body-parser')
  , api = require('./api')
  , hateoasLinker = require('express-hateoas-links');

var app = express();

app.use(bodyParser.json());
app.set('json spaces', 2);

app.use(hateoasLinker);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

app.get('/', function (req, res) {
  res.send('Nothing here');
});

app.use('/api/personnes', api);
