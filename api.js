var express = require('express'),
    elasticsearch = require('elasticsearch');

var router = express.Router();

var elasticClient = new elasticsearch.Client({
    host: 'localhost:9200',
    log: 'trace'
});

/*
CREATE
*/
router.post('/', function(req, res) {

    elasticClient.index({
        index: 'meetup',
        type: "personne",
        body: {
            prenom: req.body.prenom,
            nom: req.body.nom
        }
    }).then(function(result) {
        res.json(result, [{
            rel: "self",
            method: "GET",
            href: 'http://localhost:3000/api/personnes/' + result._id
        }, {
            rel: "personne",
            method: "GET",
            href: 'http://localhost:3000/api/personnes/' + result._id
        }])
    });
});

/*
READ
*/
router.get('/:id', function(req, res) {
    elasticClient.get({
        index: 'meetup',
        type: 'personne',
        id: req.params.id
    }).then(function(result) {
      res.json(result,[{
          rel: "self",
          method: "GET",
          href: 'http://localhost:3000/api/personnes/' + req.params.id
      }])
    });
});

module.exports = router;
